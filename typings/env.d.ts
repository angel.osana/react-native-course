declare module "@env" {
  const API_KEY: string;
  const AUTH_DOMAIN: string;
  const PROJECT_ID: string;
  const STORAGE_BUCKET: string;
  const MESSAGING_SENDER_ID: string;
  const APP_ID: string;
}
