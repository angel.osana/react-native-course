import { StyleSheet } from "react-native";

const RegisterFormStyles = StyleSheet.create({
  formContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
  },

  input: {
    width: "100%",
    marginTop: 20,
  },

  registerBtn: {
    marginTop: 20,
    width: "100%",
  },
});

export { RegisterFormStyles };
