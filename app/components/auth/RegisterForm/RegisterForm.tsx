import { Input, Icon, Button } from "@rneui/base";
import { RegisterFormStyles as styles } from "./styles";
import { useFormik } from "formik";
import { View } from "react-native";
import * as Yup from "yup";
import React, { useState } from "react";
import {
  createUserWithEmailAndPassword,
  getAuth,
  UserCredential,
} from "firebase/auth";
import { useRouter } from "../../../hooks/useRouter";
import { AccountScreensConfig } from "../../../screens/Account/config";
import Toast from "react-native-toast-message";

const initialValues = {
  email: "",
  password: "",
  confirmPassword: "",
};

const validationSchema = () => {
  return Yup.object({
    email: Yup.string()
      .email("El email no es correcto")
      .required("El email es obligatorio"),

    password: Yup.string().required("La contrasena es obligatoria"),

    confirmPassword: Yup.string()
      .required("La contrasena es obligatoria")
      .oneOf([Yup.ref("password")], "Las contrasenas tienen que ser iguales"),
  });
};

const handleRegister = async (
  formData: typeof initialValues,
  config: {
    onload: (...args: unknown[]) => unknown;
    onerror: (...args: unknown[]) => unknown;
    onsuccess: (...args: unknown[]) => unknown;
    onfinish: (...args: unknown[]) => unknown;
  }
) => {
  try {
    config.onload();
    const auth = getAuth();
    const user = await createUserWithEmailAndPassword(
      auth,
      formData.email,
      formData.password
    );

    config.onsuccess(user);
  } catch (error) {
    config.onerror(error);
  } finally {
    config.onfinish();
  }
};

const RegisterForm = () => {
  const [secureText, setSecureText] = useState(true);
  const [loading, setLoading] = useState(false);
  const router = useRouter();

  const formik = useFormik({
    initialValues,
    validationSchema: validationSchema(),
    validateOnChange: false,
    onSubmit: (data) => {
      handleRegister(data, {
        onload: () => setLoading(true),
        onerror: () => {
          Toast.show({
            type: "error",
            position: "bottom",
            text1: "Error al registrase",
          });
        },
        onsuccess: (data) => {
          router.navigate(AccountScreensConfig.stack.login.name);
        },
        onfinish: () => setLoading(false),
      });
    },
  });

  const setFieldValue = (name: keyof typeof initialValues) => {
    return (text: string) => formik.setFieldValue(name, text);
  };

  const togglePassVisibility = () => {
    setSecureText((prev) => !prev);
  };

  return (
    <View style={styles.formContainer}>
      <Input
        shake={() => {}}
        onChangeText={setFieldValue("email")}
        placeholder="Email"
        containerStyle={styles.input}
        rightIcon={<Icon type="material-community" name="at" />}
        errorMessage={formik.errors.email}
      />

      <Input
        shake={() => {}}
        onChangeText={setFieldValue("password")}
        placeholder="Password"
        containerStyle={styles.input}
        secureTextEntry={secureText}
        rightIcon={
          <Icon
            type="material-community"
            name={secureText ? "eye-off-outline" : "eye-outline"}
            onPress={togglePassVisibility}
          />
        }
        errorMessage={formik.errors.password}
      />

      <Input
        shake={() => {}}
        onChangeText={setFieldValue("confirmPassword")}
        placeholder="Confirm password"
        containerStyle={styles.input}
        secureTextEntry={secureText}
        errorMessage={formik.errors.confirmPassword}
      />

      <Button
        buttonStyle={styles.registerBtn}
        title="Sign up"
        onPress={() => formik.handleSubmit()}
        loading={loading}
      />
    </View>
  );
};

export { RegisterForm };
