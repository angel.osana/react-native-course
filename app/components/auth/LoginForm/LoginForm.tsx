import { Button, Icon, Input } from "@rneui/base";
import { useFormik } from "formik";
import { useRouter } from "../../../hooks/useRouter";
import { View } from "react-native";
import * as Yup from "yup";
import React, { useState } from "react";
import Toast from "react-native-toast-message";
import { getAuth, signInWithEmailAndPassword } from "firebase/auth";
import { AccountScreensConfig } from "../../../screens/Account/config";

const initialValues = {
  email: "",
  password: "",
};

const validationSchema = () => {
  return Yup.object({
    email: Yup.string()
      .email("El email no es correcto")
      .required("El email es obligatorio"),
    password: Yup.string().required("La contrasena es obligatoria"),
  });
};

const handleLogin = async (
  formData: typeof initialValues,
  config: {
    onload: (...args: unknown[]) => unknown;
    onerror: (...args: unknown[]) => unknown;
    onsuccess: (...args: unknown[]) => unknown;
    onfinish: (...args: unknown[]) => unknown;
  }
) => {
  try {
    config.onload();

    const auth = getAuth();

    config.onsuccess(
      await signInWithEmailAndPassword(auth, formData.email, formData.password)
    );
  } catch (error) {
    config.onerror(error);
  } finally {
    config.onfinish();
  }
};

const LoginForm = () => {
  const [isSecureText, setIsSecureText] = useState(true);
  const [loading, setLoading] = useState(false);
  const router = useRouter();

  const formik = useFormik({
    initialValues,
    validationSchema: validationSchema(),
    validateOnChange: false,
    onSubmit: (data) => {
      handleLogin(data, {
        onload: () => setLoading(true),
        onerror: () => {
          Toast.show({
            type: "error",
            position: "bottom",
            text1: "Error al iniciar sesion",
          });
        },
        onsuccess: (data) => {
          router.navigate(AccountScreensConfig.stack.index.name);
        },
        onfinish: () => setLoading(false),
      });
    },
  });

  const setFieldValue = (name: keyof typeof initialValues) => {
    return (text: string) => formik.setFieldValue(name, text);
  };

  return (
    <View style={{ marginTop: 20 }}>
      <Input
        shake={() => {}}
        placeholder={"Correo electronico"}
        rightIcon={<Icon name="at" type="material-community" />}
        onChangeText={setFieldValue("email")}
      />

      <Input
        shake={() => {}}
        placeholder={"Contrasena"}
        rightIcon={
          <Icon
            name={isSecureText ? "eye-off-outline" : "eye-outline"}
            type="material-community"
            onPress={() => setIsSecureText((prev) => !prev)}
          />
        }
        onChangeText={setFieldValue("password")}
        secureTextEntry={isSecureText}
      />

      <Button
        title={"Entrar"}
        onPress={() => formik.handleSubmit()}
        loading={loading}
      />
    </View>
  );
};

export { LoginForm };
