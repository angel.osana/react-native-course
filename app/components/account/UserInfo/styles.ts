import { StyleSheet } from "react-native";

const UserInfoStyles = StyleSheet.create({
  content: {
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
    backgroundColor: "#f2f2f2",
    paddingVertical: 30,
  },

  avatar: {
    marginRight: 20,
    backgroundColor: "green",
  },
});

export { UserInfoStyles };
