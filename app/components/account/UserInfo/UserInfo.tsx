import { View, Text } from "react-native";
import React from "react";
import { UserInfoStyles as styles } from "./styles";
import { Avatar } from "@rneui/base";
import { getAuth } from "firebase/auth";

const UserInfo = () => {
  const user = getAuth().currentUser;

  return (
    <View style={styles.content}>
      <Avatar
        size="large"
        rounded
        icon={{ type: "material", name: "person" }}
        containerStyle={styles.avatar}
      >
        <Avatar.Accessory size={24} />
      </Avatar>

      <View>
        <Text>{user ? user.displayName || "No name" : "No name"}</Text>
        <Text>{user ? user.email : "No name"}</Text>
      </View>
    </View>
  );
};

export { UserInfo };
