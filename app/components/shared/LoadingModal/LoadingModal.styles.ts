import { StyleSheet } from "react-native";

const LoadingModalStyles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },

  overlay: {
    height: 100,
    width: 200,
    backgroundColor: "#fff",
    borderColor: "#00a680",
    borderWidth: 2,
    borderRadius: 10,
  },

  text: {
    textTransform: "uppercase",
    marginTop: 6,
    color: "#00a680",
  },
});

export { LoadingModalStyles };
