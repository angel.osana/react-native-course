import { ActivityIndicator, Text, View } from "react-native";
import { LoadingModalStyles } from "./LoadingModal.styles";
import React from "react";
import { Overlay } from "@rneui/themed";

interface LoadingModalProps {
  isVisible: boolean;
  text?: string;
}
const styles = LoadingModalStyles;

export const LoadingModal = ({ isVisible = true, text }: LoadingModalProps) => {
  return (
    <Overlay overlayStyle={styles.overlay} isVisible={isVisible}>
      <View style={styles.container}>
        <ActivityIndicator size={"large"} color={"#00a680"} />
        {text ? <Text style={styles.text}>{text}</Text> : null}
      </View>
    </Overlay>
  );
};
