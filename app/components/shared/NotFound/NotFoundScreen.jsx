import React from "react";
import { Text, View } from "react-native";

export const NotFoundScreen = () => {
  return (
    <View>
      <Text style={{ fontSize: 32, textAlign: "center", paddingTop: 24 }}>
        Not Found - 404
      </Text>
    </View>
  );
};
