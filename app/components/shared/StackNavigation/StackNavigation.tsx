import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { NotFoundScreen } from "../NotFound/NotFoundScreen";
import { TabScreen } from "../../../utils";
import React from "react";

const Stack = createNativeStackNavigator();

const StackNavigatorComponent = ({ stack }: TabScreen) => {
  return () => {
    if (stack === undefined || stack.length === 0) {
      return (
        <Stack.Navigator>
          <Stack.Screen
            name={"not-found"}
            component={
              NotFoundScreen
            } /* Can be a custom default screen from props */
            options={{ title: "Pagina no encontrada" }}
          />
        </Stack.Navigator>
      );
    }

    return (
      <Stack.Navigator>
        {stack.map(({ name, component, title }) => {
          return (
            <Stack.Screen
              name={name}
              key={name}
              component={component}
              options={{ title }}
            />
          );
        })}
      </Stack.Navigator>
    );
  };
};

export { StackNavigatorComponent };
