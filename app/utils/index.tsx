import { BottomTabNavigationOptions } from "@react-navigation/bottom-tabs";
import { Icon } from "@rneui/base";
import { JSXElementConstructor } from "react";
import { ParamListBase, RouteProp } from "@react-navigation/native";
import React from "react";

type ScreensConfig = {
  tab: BaseScreenProps;
  stack: {
    [name: string]: BaseScreenProps;
  };
};

class BaseScreenProps {
  name: string;
  iconName?: string;
  title?: string;

  constructor(props: { name: string; iconName?: string; title?: string }) {
    this.name = props.name;
    this.iconName = props.iconName;
    this.title = props.title;
  }
}

class TabScreen implements BaseScreenProps {
  name: string;
  iconName?: string;
  title?: string;
  stack?: Array<StackScreen>;

  constructor(
    props: TabScreen & {
      stack: Array<StackScreen>;
    }
  ) {
    this.name = props.name;
    this.iconName = props.iconName;
    this.title = props.title;
    this.stack = props.stack;
  }
}

interface StackScreenProps extends TabScreen {
  component: JSXElementConstructor<any>;
}

class StackScreen implements StackScreenProps {
  component: JSXElementConstructor<any>;
  name: string;
  iconName?: string | undefined;
  title?: string | undefined;
  stack?: StackScreen[];

  constructor(props: StackScreenProps) {
    this.component = props.component;
    this.name = props.name;
    this.title = props.title;
    this.stack = props.stack;
  }
}

const hasInvalidMembers = (members: { [name: string]: BaseScreenProps }) => {
  const membersList = Object.values(members);
  const namesMap = {} as { [name: string]: string };

  for (const { name } of membersList) {
    if (!namesMap[name]) namesMap[name] = name;
  }

  return membersList.length !== Object.values(namesMap).length;
};

function createScreensConfig<Config extends ScreensConfig>(config: Config) {
  const { stack, tab } = config;

  if (hasInvalidMembers({ ...stack, tab })) {
    throw new Error("Screens config cannot have members with the same name");
  }

  return config;
}

const createScreensOptionsHandler = (tabsScreens: Array<TabScreen>) => {
  const icons = tabsScreens.reduce((obj, { name, iconName }) => {
    obj[name] = iconName as string;
    return obj;
  }, {} as Record<string, string>);

  return (payload: {
    route: RouteProp<ParamListBase, string>;
  }): BottomTabNavigationOptions => {
    const { route } = payload;

    return {
      tabBarActiveTintColor: "#00A680",
      headerShown: false,
      tabBarInactiveTintColor: "#646464",
      tabBarIcon: (props) => {
        const name = icons[route.name];
        return (
          <Icon
            name={name}
            color={props.color}
            size={props.size}
            type="material-community"
          />
        );
      },
    };
  };
};

export {
  TabScreen,
  StackScreen,
  BaseScreenProps,
  createScreensConfig,
  createScreensOptionsHandler,
};
