import { useNavigation } from "@react-navigation/native";
import { NativeStackNavigationProp } from "@react-navigation/native-stack";

type RootStackParamList = {
  [key: string]: Record<string, unknown> | undefined;
};

const useRouter = () => {
  const router =
    useNavigation<NativeStackNavigationProp<RootStackParamList, string>>();

  return router;
};

export { useRouter };
