import { AddRestaurantScreen } from "../Add/AddRestaurant";
import { RestaurantsScreensConfig } from ".";
import { RestaurantsScreen } from "../Main/Restaurants";
import { StackScreen, TabScreen } from "../../../utils";

const { tab, stack } = RestaurantsScreensConfig;

const indexScreen = new StackScreen({
  component: RestaurantsScreen,
  ...stack.index,
});

const addScreen = new StackScreen({
  component: AddRestaurantScreen,
  ...stack.add,
});

const RestaurantsTabScreen = new TabScreen({
  ...tab,
  stack: [indexScreen, addScreen],
});

export { RestaurantsTabScreen };
