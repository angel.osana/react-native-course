import { BaseScreenProps, createScreensConfig } from "../../../utils";

const rootTab = new BaseScreenProps({
  name: "restaurants-root",
  iconName: "compass-outline",
  title: "Restaurants",
});

const index = new BaseScreenProps({
  name: "restaurants-index",
  title: "Restaurantes",
});

const add = new BaseScreenProps({
  name: "restaurants-add",
  title: "Nuevo restaurante",
});

export const RestaurantsScreensConfig = createScreensConfig({
  tab: rootTab,
  stack: {
    index,
    add,
  },
});
