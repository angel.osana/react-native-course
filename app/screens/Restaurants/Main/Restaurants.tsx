import { AccountScreensConfig } from "../../Account/config";
import { API_KEY } from "@env";
import { Button } from "@rneui/base";
import { RestaurantsScreensConfig } from "../config";
import { useRouter } from "../../../hooks/useRouter";
import { View, Text } from "react-native";
import React from "react";

export const RestaurantsScreen = () => {
  const router = useRouter();

  const handleNavigate = () => {
    router.navigate(RestaurantsScreensConfig.stack.add.name);
  };

  const handleNavigateAccount = () => {
    router.navigate(AccountScreensConfig.tab.name, {
      screen: AccountScreensConfig.stack.index.name,
    });
  };

  return (
    <View>
      <Text>Restaurants view {API_KEY}</Text>
      <Button title={"Navigate to add restaurants"} onPress={handleNavigate} />
      <Button title={"Navigate to account"} onPress={handleNavigateAccount} />
    </View>
  );
};
