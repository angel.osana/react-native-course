import { BaseScreenProps, createScreensConfig } from "../../../utils";

const rootTab = new BaseScreenProps({
  name: "Favorites",
  iconName: "star-outline",
  title: "Favorites",
});

const FavoritesScreensConfig = createScreensConfig({
  tab: rootTab,
  stack: {},
});

export { FavoritesScreensConfig };
