import { TabScreen } from "../../../utils";
import { FavoritesScreensConfig } from ".";

const { tab } = FavoritesScreensConfig;

const FavoritesTabScreen = new TabScreen({
  stack: [],
  ...tab,
});

export { FavoritesTabScreen };
