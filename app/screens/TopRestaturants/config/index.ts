import { BaseScreenProps, createScreensConfig } from "../../../utils";

const rootTab = new BaseScreenProps({
  name: "top-restaurants",
  iconName: "star-outline",
  title: "Ranking",
});

const TopRestaurantsScreensConfig = createScreensConfig({
  tab: rootTab,
  stack: {},
});

export { TopRestaurantsScreensConfig };
