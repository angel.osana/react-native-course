import { TabScreen } from "../../../utils";
import { TopRestaurantsScreensConfig } from ".";

const { tab } = TopRestaurantsScreensConfig;

const TopRestaurantsTabScreen = new TabScreen({
  ...tab,
  stack: [],
});

export { TopRestaurantsTabScreen };
