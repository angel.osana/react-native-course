import { TabScreen } from "../../../utils";
import { SearchScreensConfig } from ".";
const { tab } = SearchScreensConfig;

const SearchTabScreen = new TabScreen({
  ...tab,
  stack: [],
});

export { SearchTabScreen };
