import { BaseScreenProps, createScreensConfig } from "../../../utils";

const rootTab = new BaseScreenProps({
  name: "search",
  iconName: "magnify",
  title: "Search",
});

const SearchScreensConfig = createScreensConfig({
  tab: rootTab,
  stack: {},
});

export { SearchScreensConfig };
