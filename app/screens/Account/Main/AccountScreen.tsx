import React, { useState, useEffect } from "react";
import { getAuth, onAuthStateChanged } from "firebase/auth";
import { AccountLoggedScreen } from "../Logged/AccountLoggedScreen";
import { AccountGuestScreen } from "../Guest/AccountGuestScreen";
import { LoadingModal } from "../../../components/shared/LoadingModal/LoadingModal";

export const AccountScreen = () => {
  const [isLogged, setIsLogged] = useState<null | boolean>(null);

  const handleAuthState = () => {
    const auth = getAuth();

    onAuthStateChanged(auth, (user) => {
      setIsLogged(Boolean(user));
    });
  };

  useEffect(() => {
    handleAuthState();
  }, []);

  if (isLogged === null) {
    return <LoadingModal isVisible text="Loading" />;
  }

  return isLogged ? <AccountLoggedScreen /> : <AccountGuestScreen />;
};
