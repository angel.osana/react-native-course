import { AccountGuestScreenStyles as styles } from "./styles";
import { AccountScreensConfig } from "../config";
import { Button } from "@rneui/base";
import { Text, ScrollView } from "react-native";
import { useRouter } from "../../../hooks/useRouter";
import React from "react";

export const AccountGuestScreen = () => {
  const { navigate } = useRouter();

  const goToLogin = () => {
    navigate(AccountScreensConfig.stack.login.name);
  };

  return (
    <ScrollView style={styles.scrollView} centerContent>
      <Text style={styles.title}>Consultar perfil de 5 tenedores</Text>

      <Text style={styles.description}>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur
        animi aperiam repellendus totam id incidunt minus natus unde placeat,
        aut, molestias doloremque magnam provident dolore nemo? Tempore deleniti
        maiores eveniet?
      </Text>

      <Button title="Go login" onPress={goToLogin} />
    </ScrollView>
  );
};
