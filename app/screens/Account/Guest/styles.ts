import { StyleSheet } from "react-native";

const AccountGuestScreenStyles = StyleSheet.create({
  scrollView: {
    marginHorizontal: 30,
  },

  title: {
    fontWeight: "bold",
    fontSize: 18,
    marginBottom: 10,
    textAlign: "center",
  },

  description: {
    textAlign: "center",
    marginBottom: 20,
  },
});

export { AccountGuestScreenStyles };
