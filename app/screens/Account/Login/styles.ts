import { StyleSheet } from "react-native";

const AccountLoginScreenStyles = StyleSheet.create({
  registerBtnContainer: {
    marginHorizontal: 40,
  },
});

export { AccountLoginScreenStyles };
