import { AccountLoginScreenStyles as styles } from "./styles";
import { AccountScreensConfig } from "../config";
import { Button } from "@rneui/base";
import { Image, Text, View } from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { LoginForm } from "../../../components/auth/LoginForm/LoginForm";
import { useRouter } from "../../../hooks/useRouter";
import React from "react";

const AccountLoginScreen = () => {
  const router = useRouter();

  return (
    <KeyboardAwareScrollView>
      <View style={styles.registerBtnContainer}>
        <Image
          style={{ width: "100%", resizeMode: "contain" }}
          source={require("../../../../assets/img/5-tenedores-letras-icono-logo.png")}
        />

        <LoginForm />

        <View style={{ marginTop: 24 }}>
          <Text
            style={{
              textAlign: "center",
              marginBottom: 10,
              fontWeight: "bold",
            }}
          >
            ¿Aun no estas registrado?
          </Text>
          <Button
            title={"Registrarse"}
            onPress={() =>
              router.navigate(AccountScreensConfig.stack.register.name)
            }
          />
        </View>
      </View>
    </KeyboardAwareScrollView>
  );
};

export { AccountLoginScreen };
