import { AccountLoggedScreenStyles as styles } from "./styles";
import { Button } from "@rneui/base";
import { getAuth, signOut } from "firebase/auth";
import { UserInfo } from "../../../components/account/UserInfo/UserInfo";
import { View } from "react-native";
import React from "react";

export const AccountLoggedScreen = () => {
  const handleLogout = () => {
    const auth = getAuth();
    signOut(auth);
  };

  return (
    <View>
      <UserInfo />

      <Button
        title="Logout"
        titleStyle={styles.logoutBtnTitle}
        buttonStyle={styles.logoutBtn}
        onPress={handleLogout}
      />
    </View>
  );
};
