import { StyleSheet } from "react-native";

const AccountLoggedScreenStyles = StyleSheet.create({
  content: { minHeight: "100%", backgroundColor: "#f2f2f2" },

  logoutBtn: {
    marginTop: 30,
    paddingVertical: 10,
    borderRadius: 0,
    backgroundColor: "#FFF",
    borderTopWidth: 1,
    borderTopColor: "#e3e3e3",
    borderBottomColor: "#e3e3e3",
  },

  logoutBtnTitle: {
    color: "#00a680",
  },
});

export { AccountLoggedScreenStyles };
