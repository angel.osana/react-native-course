import { AccountLoginScreen } from "../Login/AccountLoginScreen";
import { AccountRegisterScreen } from "../Register/AccountRegisterScreen";
import { AccountScreen } from "../Main/AccountScreen";
import { AccountScreensConfig } from ".";
import { StackScreen, TabScreen } from "../../../utils";

const { stack, tab } = AccountScreensConfig;

const indexScreen = new StackScreen({
  ...stack.index,
  component: AccountScreen,
});

const loginScreen = new StackScreen({
  ...stack.login,
  component: AccountLoginScreen,
});

const registerScreen = new StackScreen({
  ...stack.register,
  component: AccountRegisterScreen,
});

const AccountTabScreen = new TabScreen({
  ...tab,
  stack: [indexScreen, loginScreen, registerScreen],
});

export { AccountTabScreen };
