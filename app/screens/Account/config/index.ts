import { BaseScreenProps, createScreensConfig } from "../../../utils";

const rootTab = new BaseScreenProps({
  name: "account-root",
  iconName: "home-outline",
  title: "Account",
});

const index = new BaseScreenProps({
  name: "account-index",
  title: "Cuenta",
});

const login = new BaseScreenProps({
  name: "account-login",
  title: "Iniciar sesion",
});

const register = new BaseScreenProps({
  name: "account-register",
  title: "Register",
});

const AccountScreensConfig = createScreensConfig({
  tab: rootTab,
  stack: { index, login, register },
});

export { AccountScreensConfig };
