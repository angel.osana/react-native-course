import { AccountRegisterScreenStyles as styles } from "./styles";
import { RegisterForm } from "../../../components/auth/RegisterForm/RegisterForm";
import { View, Text } from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import React from "react";

const AccountRegisterScreen = () => {
  return (
    <KeyboardAwareScrollView>
      <View style={styles.form}>
        <RegisterForm />
      </View>
    </KeyboardAwareScrollView>
  );
};

export { AccountRegisterScreen };
