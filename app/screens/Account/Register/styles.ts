import { StyleSheet } from "react-native";

const AccountRegisterScreenStyles = StyleSheet.create({
  form: {
    marginHorizontal: 40,
  },
});

export { AccountRegisterScreenStyles };
