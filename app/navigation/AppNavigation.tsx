import { AccountTabScreen } from "../screens/Account/config/screens";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createScreensOptionsHandler } from "../utils";
import { FavoritesTabScreen } from "../screens/Favorites/config/screens";
import { RestaurantsTabScreen } from "../screens/Restaurants/config/screens";
import { SearchTabScreen } from "../screens/Search/config/screens";
import { StackNavigatorComponent } from "../components/shared/StackNavigation/StackNavigation";
import { TopRestaurantsTabScreen } from "../screens/TopRestaturants/config/screens";
import React from "react";

const Tab = createBottomTabNavigator();

const AllTabScreens = [
  AccountTabScreen,
  FavoritesTabScreen,
  RestaurantsTabScreen,
  SearchTabScreen,
  TopRestaurantsTabScreen,
];

const optionsHandler = createScreensOptionsHandler(AllTabScreens);

const AppNavigation = () => {
  return (
    <Tab.Navigator screenOptions={optionsHandler}>
      {AllTabScreens.map((screen, i) => {
        return (
          <Tab.Screen
            key={i}
            name={screen.name}
            options={{ title: screen.title }}
            component={StackNavigatorComponent(screen)}
          />
        );
      })}
    </Tab.Navigator>
  );
};

export default AppNavigation;
