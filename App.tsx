import { firebaseConfig } from "./app/config/firebase";
import { initializeApp } from "firebase/app";
import { LogBox } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import AppNavigation from "./app/navigation/AppNavigation";
import React from "react";
import Toast from "react-native-toast-message";
LogBox.ignoreAllLogs();

initializeApp(firebaseConfig);

export default function App() {
  return (
    <>
      <NavigationContainer>
        <AppNavigation />

        <Toast />
      </NavigationContainer>
    </>
  );
}
